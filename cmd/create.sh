rename_function cmd_create orig_cmd_create
cmd_create() {
    orig_cmd_create \
        "$@"    # accept additional options, e.g.: --privileged
}
