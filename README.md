# Minimal Ubuntu container

## Installation

  - First install `ds`: https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull ubuntu`

  - Create a directory for the container: `ds init ubuntu @ubuntu1`

  - Fix the settings: `cd /var/ds/ubuntu1/ ; vim settings.sh`

  - Make the container: `ds make`

## Other commands

```
ds stop
ds start
ds shell
ds help
```
